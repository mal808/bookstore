This is a java project, using Eclipse IDE

**Eclipse version:**   
Mars.2 Release (4.5.2)    
  
**Git version:**        
2.9.0.windows.1    

**Maven version:**        
3.2.5    

**Java:**             
1.8.0_92  

**JRE:**             
1.8.0_92-b14  

Although this project is configured for Maven, I didn't use Maven in the end. 
There are no third party packages used in this application.