package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import bookstore.ShopFacade;
import interfaces.BookList;
import stock.Book;

public class CommandLineUI {

	public static final int UI_MENU_OFFSET = 1;

	private String lineBreak = "*********************************";
	private String shopSign = "\n\n\n*********************************\n* Welcome to Contribe Book Shop *\n*********************************\n\n\n";
	private String buySign = "\n\n\n*********************************\n*        Buying Books           *\n*********************************\n\n\n";
	private String adminSign = "\n\n\n*******************************\n*       Administration          *\n*********************************\n\n\n";
	private String searchSign = "\n\n\n*******************************\n*        Search Books           *\n*********************************\n\n\n";
	private String choose = "Choose an option:";
	private String searchPrompt = "You can search by title and author, Enter a comma seperated list of titles and/or authors:";
	private String mainOptions = "1. Buy books \n2. Administration \n3. quit \nEnter a number: ";
	private String buyBooksOpt = "1. List all books \n2. Search for books by title and/or author \n3. Manage Shopping Basket \n4. Main Menu \nEnter a number:";
	private String adminOpt = "1. Add a new item to stock \n2. Main Menu \nEnter a number";
	private String imSorry = "I'm sorry, \"";
	private String invalidOpt = "\" does not look like valid option, please try again";
	private String addBookPrompt = "To add a book to your basket, please enter the item number or numbers seperated by a comma: ";
	private String yourBasketContains = "Your shopping basket contains the following Items: ";
	private String removeFromBasket = "To remove items, enter an item number or numbers seperated by a comma, otherwise type \"buy\" to buy the items or press enter to return to the main menu.";
	private String thanksForVisiting = "Thankyou for visiting Contribe Book Shop, please come back soon!";
	private String enterTitle = "Enter the book title:";
	private String enterAuthor = "Enter the book author:";
	private String enterPrice = "Enter the book price in SEK";
	private String enterQuantity = "Enter the amount of stock to add to the system:";
	private String bookAdded = "\nBook successfully added!\n";
	private String weHaveTheFollowing = "We have the following books on our records: ";
	private String hasBeenPurchased = "\" has been purchased for ";
	private String notInStock = "\" is not currently in Stock, you have not been charged for this book";
	private String notOnSystem = "\" is not on our system, you have not been charged for this book";
	private String purchaseComplete = "\n\n !! Purchase Complete !!\n\n";

	private ShopFacade shop;
	private BufferedReader reader;
	private String userInput = "";

	public CommandLineUI() {
		shop = new ShopFacade();
		reader = new BufferedReader(new InputStreamReader(System.in));
	}

	public void run() {
		boolean running = true;

		while (running) {
			System.out.println(shopSign);
			System.out.println(choose);
			System.out.println(mainOptions);

			try {
				userInput = reader.readLine();
				int option = Integer.parseInt(userInput);

				switch (option) {

				case 1:
					buyBooks();
					break;

				case 2:
					doAdmin();
					break;

				case 3:
					System.out.println(thanksForVisiting);
					System.exit(0);

				default:
					System.out.println(imSorry + "\"" + userInput + invalidOpt);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				System.out.println(imSorry + userInput + invalidOpt);
			} catch (NumberFormatException e) {
				System.out.println(imSorry + "\"" + userInput + invalidOpt);
			}
		}
	}

	/**
	 * Handles the "Buy Books" sub menu
	 */
	private void buyBooks() {
		boolean running = true;

		while (running) {
			System.out.println(buySign);
			System.out.println(buyBooksOpt);

			try {
				userInput = reader.readLine();

				int option = Integer.parseInt(userInput);

				switch (option) {

				case 1:
					listAllBooks();

					break;

				case 2:
					bookSearch();
					break;

				case 3:
					manageShoppingBasket();
					break;

				case 4:
					running = false;
					break;

				default:
					System.out.println(imSorry + "\"" + userInput + invalidOpt);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				System.out.println(imSorry + userInput + invalidOpt);
			} catch (NumberFormatException e) {
				System.out.println(imSorry + "\"" + userInput + invalidOpt);
			}
		}
	}

	/**
	 * Handles the "Administration" sub menu
	 */
	private void doAdmin() {
		boolean running = true;

		while (running) {
			System.out.println(adminSign);
			System.out.println(adminOpt);

			try {
				userInput = reader.readLine();
				int option = Integer.parseInt(userInput);

				switch (option) {

				case 1:
					System.out.println(enterTitle);
					String title = reader.readLine();
					System.out.println(enterAuthor);
					String author = reader.readLine();
					System.out.println(enterPrice);
					String price = reader.readLine();
					price = price.replace(",", "");
					BigDecimal bd = new BigDecimal(Double.parseDouble(price));
					System.out.println(enterQuantity);
					String quantity = reader.readLine();
					quantity = quantity.replace(",", "");
					int qty = Integer.parseInt(quantity);

					shop.addBook(title, author, bd, qty);
					System.out.println(bookAdded);
					break;

				case 2:
					running = false;
					break;

				default:
					System.out.println(imSorry + userInput + invalidOpt);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				System.out.println(imSorry + userInput + invalidOpt);
			} catch (NumberFormatException e) {
				System.out.println(imSorry + "\"" + userInput + invalidOpt);
			}
		}
	}

	private void listAllBooks() {
		Book[] books = shop.getBookList(null);
		processUserSelection(books);
	}

	private void bookSearch() {
		System.out.println(searchSign);
		System.out.println(searchPrompt);

		try {
			String searchString = reader.readLine();
			Book[] books = shop.getBookList(searchString);
			processUserSelection(books);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Presents the user with a list of books and processes their resulting
	 * selection, adding books to their basket
	 * 
	 * @param books
	 *            A list of Books
	 */
	private void processUserSelection(Book[] books) {
		System.out.println(weHaveTheFollowing);

		for (int i = 0; i < books.length; i++) {

			System.out.println(lineBreak);
			System.out.println("Item Number: " + (i + UI_MENU_OFFSET) + "\n ");
			System.out.println(books[i]);
			System.out.println();

		}

		System.out.println(addBookPrompt);

		try {
			String userInput = reader.readLine();
			String[] itemArray = userInput.split(",");
			int[] itemNumbers = new int[itemArray.length];

			for (int i = 0; i < itemArray.length; i++) {
				itemNumbers[i] = Integer.parseInt(itemArray[i]) - UI_MENU_OFFSET;
			}

			for (int itemNo : itemNumbers) {

				if (itemNo >= 0 && itemNo < (books.length + UI_MENU_OFFSET)) {
					shop.addBookToShoppingBasket(books[itemNo]);
				} else {
					System.out.println("Item : " + (itemNo + UI_MENU_OFFSET) + " is not a valid item");
				}
			}

			System.out.println("Your basket now has a total of " + shop.getShoppingBasketCount() + " Items.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			System.out.println(imSorry + userInput + invalidOpt);
		} catch (NumberFormatException e) {
			System.out.println(imSorry + "\"" + userInput + invalidOpt);
		}
	}

	private void manageShoppingBasket() {
		boolean running = true;

		while (running) {
			System.out.println(yourBasketContains);
			Book[] books = shop.getShoppingBasketItems();
			BigDecimal total = new BigDecimal(0d);

			for (int i = 0; i < books.length; i++) {

				System.out.println(lineBreak);
				System.out.println("Item Number: " + (i + UI_MENU_OFFSET) + "\n ");
				System.out.println(books[i]);
				total = total.add(books[i].getPrice());
			}

			System.out.println();
			System.out.println(lineBreak);
			System.out.println("TOTAL: " + total + " SEK");
			System.out.println(lineBreak);
			System.out.println(removeFromBasket);
			String userInput = "";

			try {
				userInput = reader.readLine();

				if (userInput.equals("buy")) {
					int[] results = shop.buy();
					processResults(books, results);
					running = false;
				} else if (userInput.length() > 0) {
					String[] itemArray = userInput.split(",");
					int[] itemNumbers = new int[itemArray.length];

					for (int i = 0; i < itemArray.length; i++) {
						itemNumbers[i] = Integer.parseInt(itemArray[i]) - UI_MENU_OFFSET;
					}

					for (int index : itemNumbers) {
						shop.removeItemFromShoppingBasket(index);
					}

				} else {
					running = false;
				}

			} catch (IndexOutOfBoundsException e) {
				System.out.println(imSorry + userInput + invalidOpt);
				running = false;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				System.out.println(imSorry + userInput + invalidOpt);
			}
		}

	}

	private void processResults(Book[] books, int[] results) {
		BigDecimal total = new BigDecimal(0d);
		System.out.println(purchaseComplete);
		
		for (int i = 0; i < results.length; i++) {

			switch (results[i]) {

			case BookList.OK:
				System.out.println("* \"" + books[i].getTitle() + hasBeenPurchased + books[i].getPrice() + " SEK");
				total = total.add(books[i].getPrice());
				break;

			case BookList.NOT_IN_STOCK:
				System.out.println("* \"" + books[i].getTitle() + notInStock);

				break;

			case BookList.DOES_NOT_EXIST:
				System.out.println("* \"" + books[i].getTitle() + notOnSystem);
			}
		}
		
		shop.emptyShoppingBasket();
		System.out.println();
		System.out.println("Your total costs for this shop is: " + total + " SEK\n\n");
		System.out.println("Thankyou for shopping with Contribe!");
		System.out.println(lineBreak);
		System.out.println();

	}
}
