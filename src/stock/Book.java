package stock;

import java.math.BigDecimal;

public class Book {

	/**
	 * The book title
	 */
	private String title;

	/**
	 * The book author
	 */
	private String author;

	/**
	 * The price of the book in Swedish Kronor
	 */
	private BigDecimal price;

	/**
	 * The amount of books currently in Stock
	 */
	private int quantity;

	public Book(String title, String author, BigDecimal price) {
		this.title = title;
		this.author = author;
		this.price = price;
	}

	/**
	 * 
	 * @param title
	 *            The book Title
	 * @param author
	 *            The book Author
	 * @param price
	 *            The price in Swedish Kronor
	 * @param quantity
	 *            The amount available for sale
	 */
	public Book(String title, String author, BigDecimal price, int quantity) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.quantity = quantity;
	}

	/**
	 * @return the title of the book
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title of the book
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the author of the book
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author of the book
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the price in Swedish Kronor
	 */

	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price in Swedish Kronor
	 */

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	@Override
	public String toString() {
		return "Title: " + title + "\nAuthor: " + author + "\nPrice: " + price + " SEK";
	}
}
