package bookstore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import stock.Book;

/**
 * This class is used to load data from various sources into the Bookstore
 * 
 * @author Paul Mallon
 *
 */
public class DataLoader {

	/**
	 * This URL is used as the source of data if loadUrlData() is passed a null
	 * value
	 */
	URL defaultURL;
	URLConnection urlConnection;
	ArrayList<String> rawData;
	ArrayList<Book> stockData;

	public DataLoader() {
		rawData = new ArrayList<String>();
		stockData = new ArrayList<Book>();

		try {
			defaultURL = new URL("http://www.contribe.se/bookstoredata/bookstoredata.txt");

		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Loads data from the passed URL object. If null is passed then a default
	 * URL is used
	 * 
	 * @param url
	 *            the URL location of the data to be parsed
	 * @return true if successful, false otherwise
	 */
	public ArrayList<Book> loadUrlData(URL url) {

		if (url == null) {
			return loadData(defaultURL);
		} else {
			return loadData(url);
		}

	}

	/**
	 * Loads data from the passed URL object, replaces all current data.
	 * 
	 * @param url
	 *            the URL location of the data to be parsed
	 * @return true if successful, false otherwise
	 */
	private ArrayList<Book> loadData(URL url) {

		try {
			urlConnection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				rawData.add(inputLine);
			}

			for (String row : rawData) {
				String[] rowData = row.split(";");
				String param1 = rowData[0];
				String param2 = rowData[1];
				String priceString = rowData[2];
				priceString = priceString.replaceAll(",", "");
				BigDecimal price = BigDecimal.valueOf((Double) Double.valueOf(priceString));
				int quantity = Integer.valueOf(rowData[3]);

				stockData.add(new Book(param1, param2, price, quantity));
			}
			
			in.close();
			
			return stockData;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
