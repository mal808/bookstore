package bookstore;

import java.math.BigDecimal;

import stock.Book;

public class ShopFacade {

	private BookStore store;
	private ShoppingBasket basket;
	private Book[] bookList = null;

	public ShopFacade() {
		store = new BookStore();
		basket = new ShoppingBasket();
	}

	/**
	 * Adds a new Book to the system
	 * 
	 * @param title
	 *            The title of the book
	 * @param author
	 *            The author of the book
	 * @param price
	 *            The price of the item in Swedish Kronor
	 * @param quantity
	 *            The amount of items to add to the stock
	 */
	public void addBook(String title, String author, BigDecimal price, int quantity) {
		Book item = new Book(title, author, price);
		store.add(item, quantity);
	}

	/**
	 * 
	 * @param searchString
	 *            A comma separated string of titles and/or authors
	 * @return A list of available books
	 */
	public Book[] getBookList(String searchString) {
		bookList = store.list(searchString);
		return bookList;
	}

	/**
	 * 
	 * @param index
	 *            The index of the item to be removed
	 */
	public void removeItemFromShoppingBasket(int index) {
		basket.removeItem(index);
	}

	/**
	 * 
	 * @param book
	 *            The book to add to the ShoppingBasket
	 */
	public void addBookToShoppingBasket(Book book) {
		basket.addItem(book);
	}

	/**
	 * 
	 * @return The number of elements in the ShoppingBasket
	 */
	public int getShoppingBasketCount() {
		return basket.size();
	}

	/**
	 * 
	 * @return The contents of the ShoppingBasket
	 */
	public Book[] getShoppingBasketItems() {
		return basket.getAllItems().toArray(new Book[basket.getAllItems().size()]);
	}

	/**
	 * 
	 * @return An array of ints representing the status of each purchase
	 */
	public int[] buy() {
		return store.buy(basket.getAllItems().toArray(new Book[basket.getAllItems().size()]));
	}
	
	/**
	 * Replaces the current ShoppingBasket with a new empty ShoppingBasket
	 */
	public void emptyShoppingBasket() {
		basket = new ShoppingBasket();
	}
}
