package bookstore;
import java.util.ArrayList;
import stock.Book;

public class ShoppingBasket {

	private ArrayList<Book> shoppingList;
	
	public ShoppingBasket(){
		shoppingList = new ArrayList<Book>();
	}
	
	/**
	 * Adds an Item to the shopping basket
	 * @param item A Book
	 * @return true if successfully added, false otherwise
	 */
	public boolean addItem(Book item){
		return shoppingList.add(item);
	}
	
	/**
	 * Removes a Book from the shopping basket
	 * @param index the index of the item to be removed
	 * @return The Book that has been removed from the shopping basket
	 */
	public Book removeItem(int index){
		return shoppingList.remove(index);
	}
	
	/**
	 * Returns all Books currently in the shopping basket
	 * @return ArrayList<Book> 
	 */
	public ArrayList<Book> getAllItems(){
		return shoppingList;
	}
	
	/**
	 * 
	 * @return The number of items in the ShoppingBasket
	 */
	public int size(){
		return shoppingList.size();
	}

}
