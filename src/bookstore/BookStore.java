package bookstore;

import java.util.ArrayList;

import interfaces.BookList;
import stock.Book;

public class BookStore implements BookList {

	DataLoader loader;
	ArrayList<Book> stockList;

	public BookStore() {
		loader = new DataLoader();
		stockList = loader.loadUrlData(null);
	}

	public Book[] list(String searchString) {
		ArrayList<Book> books = new ArrayList<Book>();
		Book[] list = null;
		
		if (searchString != null && !searchString.equals("")) {
			String[] searchParams = searchString.split(",");
			
			
			for (String searchParam : searchParams) {
				
				for (Book book : stockList) {

					if (compare(book, searchParam)) {
						books.add(book);
					}
					
				}
				
			}
			
			list = books.toArray(new Book[books.size()]);

		} else {

			list = stockList.toArray(new Book[stockList.size()]);
		}

		return list;
	}

	public boolean add(Book book, int quantity) {
		book.setQuantity(quantity);
		return stockList.add((Book) book);
	}

	public int[] buy(Book... books) {
		int[] results = new int[books.length];

		basketIterator: for (int i = 0; i < books.length; i++) {
			Book book = books[i];
			
			for (Book item : stockList) {

				if (item == book && item.getQuantity() > 0) {
					item.setQuantity(item.getQuantity() - 1);
					results[i] = BookList.OK;
					continue basketIterator;
				} else if (item == book && item.getQuantity() == 0) {
					results[i] = BookList.NOT_IN_STOCK;
					continue basketIterator;
				}
				
			}
			
			results[i] = BookList.DOES_NOT_EXIST;
		}
		
		return results;

	}

	/**
	 * Compares the Title and Author of the book parameter to the value of the
	 * search parameter
	 * 
	 * @param book
	 *            the book to compare
	 * @param searchParam
	 *            The search parameter - book title or author
	 * @return true if the title or author is found, false otherwise
	 */
	private boolean compare(Book book, String searchParam) {

		if (book.getTitle().toLowerCase().trim().equals(searchParam.toLowerCase().trim())
				|| book.getAuthor().toLowerCase().trim().equals(searchParam.toLowerCase().trim())) {
			return true;
		} else {
			return false;
		}
		
	}

}
