package tests;

import java.math.BigDecimal;

import org.junit.Test;

import bookstore.ShopFacade;
import stock.Book;

public class TestShopFacade {

	String title = "Life the Universe and Everything";
	String author = "Douglas Adams";
	BigDecimal price = new BigDecimal(42000d);
	int quantity = 42;
	Book book = new Book(title, author, price, quantity);
	
	@Test
	public void testAddBook() {	
		ShopFacade shop = new ShopFacade();
		String title = "Life the Universe and Everything";
		String author = "Douglas Adams";
		BigDecimal price = new BigDecimal(42000d);
		int quantity = 42;
		Book book = new Book(title, author, price, quantity);
		shop.addBook(title, author, price, quantity);
		Book[] list = shop.getBookList(book.getTitle());
		assert(list[0] == book);
	}

	@Test
	public void testGetBookList() {	
		ShopFacade shop = new ShopFacade();
		Book[] list = shop.getBookList(null);
		assert(list.length > 0);
		
		shop.addBook(title, author, price, quantity);
		list = shop.getBookList(author);
		assert(list[0] == book);
	}

	@Test
	public void testRemoveItemFromShoppingBasket() {	
		ShopFacade shop = new ShopFacade();
		shop.addBookToShoppingBasket(book);
		Book[] list = shop.getShoppingBasketItems();
		assert(list[0] == book);
		
		shop.removeItemFromShoppingBasket(0);
		list = shop.getShoppingBasketItems();
		assert(list.length == 0);
	}

	@Test
	public void testAddBookToShoppingBasket() {	
		ShopFacade shop = new ShopFacade();
		shop.addBookToShoppingBasket(book);
		Book[] list = shop.getShoppingBasketItems();
		assert(list[0] == book);
	}

	@Test
	public void testGetShoppingBasketCount() {	
		ShopFacade shop = new ShopFacade();
		shop.addBookToShoppingBasket(book);
		shop.addBookToShoppingBasket(book);
		int count = shop.getShoppingBasketCount();
		assert(count == 2);
	}

	@Test
	public void testGetShoppingBasketItems() {	
		ShopFacade shop = new ShopFacade();
		shop.addBookToShoppingBasket(book);
		shop.addBookToShoppingBasket(book);
		shop.addBookToShoppingBasket(book);
		Book[] list = shop.getShoppingBasketItems();
		assert(list.length == 3);
		assert(list[0] == book);
		assert(list[1] == book);
		assert(list[2] == book);
	}

	@Test
	public void testBuy() {		
		ShopFacade shop = new ShopFacade();
		Book[] list = shop.getBookList(null);
		int startQuantity = list[0].getQuantity();
		shop.addBookToShoppingBasket(list[0]);
		shop.buy();
		list = shop.getBookList(null);
		assert(list[0].getQuantity() == (startQuantity - 1));
	}

	@Test
	public void testSearchForBooks() {	
		ShopFacade shop = new ShopFacade();
		Book[] list = shop.getBookList(null);
		Book book = list[0];
		list = null;
		list = shop.getBookList(book.getTitle());
		assert(list[0] == book);
	}

	@Test
	public void testEmptyShoppingBasket() {	
		ShopFacade shop = new ShopFacade();
		Book[] list = shop.getBookList(null);
		shop.addBookToShoppingBasket(list[0]);
		Book[] basket = shop.getShoppingBasketItems();
		assert(basket.length == 1); 
		
		shop.emptyShoppingBasket();
		basket = shop.getShoppingBasketItems();
		assert(basket.length == 0);
	}

}
