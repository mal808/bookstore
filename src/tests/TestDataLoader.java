/**
 * 
 */
package tests;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Test;

import bookstore.DataLoader;
import stock.Book;

/**
 * @author Paul Mallon
 *
 */
public class TestDataLoader {

	/**
	 * @throws java.lang.Exception
	 */


	@Test
	public void test() {
		String title = "Mastering åäö";
		String author = "Average Swede";
		BigDecimal cost = new BigDecimal(762.00d);;
		int quantity = 15;
		DataLoader loader = new DataLoader();
		ArrayList<Book> books = loader.loadUrlData(null);
		Book book = books.get(0);	
		assert(book.getTitle().equals(title));
		assert(book.getAuthor().equals(author));
		assert(book.getPrice().compareTo(cost) == 0);
		assert(book.getQuantity() == quantity);
		
		title = "Desired";
		author = "Rich Bloke"; 
		cost = new BigDecimal(564.50d);
		quantity = 0;	
		book = books.get(6);	
		assert(book.getTitle().equals(title));
		assert(book.getAuthor().equals(author));
		assert(book.getPrice().compareTo(cost) == 0);
		assert(book.getQuantity() == quantity);
		
	}

}
