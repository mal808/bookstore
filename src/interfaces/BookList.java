package interfaces;

import stock.Book;

public interface BookList {

	/**
	 * Returned by BookList.buy() to confirm a book has been bought
	 */
	public static final int OK = 0;

	/**
	 * Returned by BookList.buy() if a book is not in stock
	 */
	public static final int NOT_IN_STOCK = 1;

	/**
	 * Returned by BookList.buy() if a book does not exist on the system
	 */
	public static final int DOES_NOT_EXIST = 2;

	/**
	 * Returns a list of Book items
	 * @param searchString The books to search for pass in a comma separated String of book titles
	 * @return Book[] An array of Book items
	 */
	public Book[] list(String searchString);

	/**
	 * Adds a book to the list of available books
	 * @param book The book to add
	 * @param quantity The quantity of books available
	 * @return boolean true if addition was successful
	 */
	public boolean add(Book book, int quantity);

	/**
	 * Accepts a String list or String[] and returns status codes OK,
	 * NOT_IN_STOCK or DOES_NOT_EXIST. See the BookList javadoc for
	 * further details.
	 * 
	 * @param books a list of books
	 * @return An int[] containing the status of each book. 
	 */
	public int[] buy(Book... books);
}
