package interfaces;

/**
 * This interface provides common methods for all shop items, and should be
 * implemented by all items available for sale in the shop.
 * 
 * @author Paul Mallon
 *
 */
public interface StockItemInterface {

	/**
	 * 
	 * @return A String representing the Item type, for example "Book" or
	 *         "Camera"
	 */
	public String getItemType();

}
